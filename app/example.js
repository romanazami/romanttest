angular.module('plunker', ['ui.bootstrap']);
function AccordionDemoCtrl($scope) {

  $scope.groups = [
    {
      title: "•	Experience programming in object-oriented languages such as C/C++, Java, Python, Go is preferred",
      content: "Dynamic Group Body - 1",
      open: false
    },
    {
      title: "•	Knowledge of Unix/Linux environment and general networking concepts",
      content: "This website is running on an Ubuntu Linux operating system that I set up.",
      open: false
    },
    {
      title: "•	Experience with Amazon Web Services (EC2, VPC, ELB, S3, CloudFormation, etc) is a plus",
      content: "This website is an EC2 instance with an autoscaling group and ELB. My picture is being pulled directly from an S3 bucket.",
      open: false
    },
    {
      title: "•	Experience in Agile/scrum methodologies with software development life-cycle experience is preferred",
      content: "",
      open: false
    },
    {
      title: "•	0-3 years of software development",
      content: "I have been working for Prepared Response for the past 7 months as a Full Stack developer. I have developed applications in C# to angularjs. ",
      open: false
    }
  ];
  
  $scope.addNew = function() {
    $scope.groups.push({
      title: "New One Created",
      content: "Dynamically added new one",
      open: false
    });
  }
  
}
